#!/usr/bin/python

import re
import json
from urllib.error import HTTPError
from ansible.module_utils.basic import AnsibleModule
from ansible.module_utils.urls import open_url

ANSIBLE_METADATA = {
    'metadata_version': '1.1',
    'status': ['preview'],
    'supported_by': 'community'
}

DOCUMENTATION = '''
---
module: gitea_mirror

short_description: Create Gitea mirror for a repository

version_added: "2.9"

description:
    - "Set up a mirror for a remote repository using an existing Gitea installation"

options:
    name:
        gitea_config:
            - Details about the Gitea installation
        required: true
    new:
        repository:
            - Details about the repository to be cloned
        required: true

author:
    - Bence Hornák (@hornakbence)
'''

EXAMPLES = '''
- name: Mirror public repository
  gitea_mirror:
    gitea_config:
      username: example_user
      password: example_password
    repository:
      url: https://gitlab.com/bence.hornak/ansible-backup-collection.git
'''

RETURN = '''
'''


def main():

    module_args = dict(
        gitea_config=dict(type='dict', required=True, options=dict(
            hostname=dict(type='str', default='http://localhost:3000'),
            username=dict(type='str', required=True),
            password=dict(type='str', required=True),
        )),
        repository=dict(type='dict', required=True, options=dict(
            name=dict(type='str'),
            url=dict(type='str', required=True),
            auth_username=dict(type='str'),
            auth_password=dict(type='str'),
        )),
    )

    result = dict(
        changed=False,
    )

    module = AnsibleModule(
        argument_spec=module_args,
        supports_check_mode=True
    )

    gitea_config = module.params['gitea_config']
    repo_config = module.params['repository']
    if not repo_config['name']:
        repo_config['name'] = _get_default_name(repo_config['url'])
    repo_config['owner_uid'] = _get_user_id(module, gitea_config)

    repo_exists = _repo_exists(module, gitea_config, repo_config['name'])
    result['changed'] = not repo_exists

    if module.check_mode:
        module.exit_json(**result)

    if not repo_exists:
        _create_repo(module, gitea_config, repo_config)

    module.exit_json(**result)


def _get_default_name(url):
    return re.fullmatch('^.+/([^/]+)(?:\\.git)$', url).group(1)


def _get_user_id(module, gitea_config):
    response = json.loads(_api_call(module, gitea_config, '/user').read())
    return response['id']


def _repo_exists(module, gitea_config, name):
    try:
        _api_call(module, gitea_config,
                  '/repos/{}/{}'.format(gitea_config['username'],
                                        name))
    except HTTPError:
        return False
    else:
        return True


def _create_repo(module, gitea_config, repo_config):
    _api_call(module, gitea_config,
              '/repos/migrate',
              json={
                  'clone_addr': repo_config['url'],
                  'repo_name': repo_config['name'],
                  'uid': repo_config['owner_uid'],
                  'auth_username': repo_config['auth_username'],
                  'auth_password': repo_config['auth_password'],
                  'mirror': True,
                  'private': True,
              },
              method='POST')


def _api_call(module, gitea_config, endpoint, json=None, **kwargs):
    if json:
        kwargs['data'] = module.jsonify(json)
        kwargs['headers'] = kwargs.get('headers', {})
        kwargs['headers']['Content-Type'] = 'application/json'

    return open_url('{}/api/v1{}'.format(gitea_config['hostname'], endpoint),
                    url_username=gitea_config['username'],
                    url_password=gitea_config['password'],
                    force_basic_auth=True,
                    **kwargs
                    )


if __name__ == '__main__':
    main()
